# Implementation of Adaptive Mixture of Experts
# Copyright (C) 2020  Abien Fred Agarap
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import json
import os
import random
from typing import List, Tuple

import numpy as np
import torch

__author__ = "Abien Fred Agarap"
__version__ = "1.0.0"


def get_hyperparameters(hyperparameters_path: str) -> Tuple:
    """
    Returns hyperparameters from JSON file.

    Parameters
    ----------
    hyperparameters_path: str
        The path to the hyperparameters JSON file.

    Returns
    -------
    Tuple
        dataset: str
            The name of the dataset to use.
        batch_size: int
            The mini-batch size.
        epochs: int
            The number of training epochs.
        learning_rate: float
            The learning rate to use for optimization.
        input_dim: int
            The dimensionality of the input feature channel.
        num_classes: int
            The number of classes in a dataset.
    """
    with open(hyperparameters_path, "r") as file:
        config = json.load(file)

    dataset = config.get("dataset")
    assert isinstance(dataset, str), "[dataset] must be [str]."

    batch_size = config.get("batch_size")
    assert isinstance(batch_size, int), "[batch_size] must be [int]."

    epochs = config.get("epochs")
    assert isinstance(epochs, int), "[epochs] must be [int]."

    learning_rate = config.get("learning_rate")
    assert isinstance(learning_rate, float), "[learning_rate] must be [float]."

    num_experts = config.get("num_experts")
    assert isinstance(num_experts, int), "[num_experts] must be [int]."

    input_dim = config.get("input_dim")
    assert isinstance(input_dim, int), "[input_dim] must be [int]."

    num_classes = config.get("num_classes")
    assert isinstance(num_classes, int), "[num_classes] must be [int]."

    hyperparameters_filename = os.path.basename(hyperparameters_path)
    hyperparameters_filename = hyperparameters_filename.lower()
    if "dnn" in hyperparameters_filename:
        units = config.get("units")
        assert isinstance(units, List), "[units] must [List]."
        assert len(units) >= 2, "len(units) must be >= 2."
        return (
            units,
            dataset,
            batch_size,
            epochs,
            learning_rate,
            num_experts,
            input_dim,
            num_classes,
        )
    return (
        dataset,
        batch_size,
        epochs,
        learning_rate,
        num_experts,
        input_dim,
        num_classes,
    )
