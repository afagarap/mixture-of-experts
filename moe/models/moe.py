# Implementation of Adaptive Mixture of Experts
# Copyright (C) 2020--present  Abien Fred Agarap
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Implementation of Mixture-of-Experts model"""
from typing import List, Tuple, Union

import lightning
import torch
from torchmetrics import Accuracy

from moe.models.base import Base


class Expert(torch.nn.Module):
    """
    A feed-forward neural network that optimizes
    softmax cross entropy using a gradient-based method.
    """

    def __init__(
        self,
        units: Union[List, Tuple] = [(389, 500), (500, 500), (500, 1)],
    ):
        """
        Constructs a feed-forward neural network.

        Parameters
        ----------
        units: Union[List, Tuple]
            An iterable that consists of the number of units in each hidden layer.
        """
        super().__init__()
        self.model = torch.nn.Sequential(torch.nn.Flatten())
        for index, (in_features, out_features) in enumerate(units):
            self.model.add_module(
                f"linear-{index}",
                torch.nn.Linear(in_features=in_features, out_features=out_features),
            )
            if index < (len(units) - 1):
                self.model.add_module(
                    f"activation-{index}", torch.nn.Mish(inplace=True)
                )

    def forward(self, features: torch.Tensor) -> torch.Tensor:
        """
        Defines the forward pass by the model.

        Parameter
        ---------
        features: torch.Tensor
            The input features.

        Returns
        -------
        logits: torch.Tensor
            The model output.
        """
        logits = self.model(features)
        return logits


class Gating(torch.nn.Module):
    """
    A feed-forward neural network that optimizes
    softmax cross entropy using a gradient-based method,
    to classify which model to use for a data.
    """

    def __init__(
        self,
        units: Union[List, Tuple] = [(389, 500), (500, 500)],
        num_experts: int = 3,
    ):
        """
        Constructs a feed-forward neural network.

        Parameters
        ----------
        units: Union[List, Tuple]
            An iterable that consists of the number of units in each hidden layer.
        num_experts: int
            The number of experts to classify.
        """
        super().__init__()

        units.append((units[-1][-1], num_experts))

        self.model = torch.nn.Sequential(torch.nn.Flatten())
        for index, (in_features, out_features) in enumerate(units):
            self.model.add_module(
                f"linear-{index}",
                torch.nn.Linear(in_features=in_features, out_features=out_features),
            )
            if index < (len(units) - 1):
                self.model.add_module(
                    f"activation-{index}", torch.nn.Mish(inplace=True)
                )
            elif index == (len(units) - 1):
                self.model.add_module("classification", torch.nn.Softmax(1))

    def forward(self, features: torch.Tensor) -> torch.Tensor:
        """
        Defines the forward pass by the model.

        Parameter
        ---------
        features: torch.Tensor
            The input features.

        Returns
        -------
        expert_choices: torch.Tensor
            The probabilities assigned for each expert,
            for each input features.
        """
        expert_choices = self.model(features)
        return expert_choices


class MoE(Base):
    def __init__(
        self,
        learning_rate: float = 1e-3,
        num_experts: int = 3,
        units: Union[List, Tuple] = [[389, 500], [500, 500], [500, 1]],
    ):
        """
        Builds a Mixture-of-Experts model that consists of
        single-layer neural network for both the expert and the gating networks.

        Parameters
        ----------
        learning_rate: float
            The learning rate to use for model weights optimization.
        num_experts: int
            The number of experts to instantiate.
        units: Union[List, Tuple]
            An iterable of hidden layer units.
        """
        super().__init__()
        self.num_experts = num_experts
        self.experts = torch.nn.ModuleList([Expert(units) for _ in range(num_experts)])
        self.gating = Gating(units=units[:-1], num_experts=num_experts)

        if units[-1][-1] == 1:
            accuracy_args = dict(task="binary")
        else:
            accuracy_args = dict(task="multiclass", num_classes=units[-1][-1])

        self.accuracy = Accuracy(**accuracy_args)

        self.criterion = (
            torch.nn.BCEWithLogitsLoss()
            if accuracy_args["task"] == "binary"
            else torch.nn.CrossEntropyLoss()
        )

        self.learning_rate = learning_rate

    def forward(self, features: torch.Tensor) -> torch.Tensor:
        expert_outputs = list()
        for expert in self.experts:
            expert_outputs.append(expert(features))
        expert_outputs = torch.stack(expert_outputs, dim=1)
        gate_outputs = self.gating(features).unsqueeze(dim=2)
        weighted_outputs = expert_outputs * gate_outputs
        mixture_output = torch.sum(weighted_outputs, dim=1)
        return mixture_output
