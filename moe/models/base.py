# Implementation of Adaptive Mixture of Experts
# Copyright (C) 2020--present  Abien Fred Agarap
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Implementation of a base PyTorch Lightning model"""
from typing import List, Tuple, Union

import lightning
import torch
from torchmetrics import Accuracy


class Base(lightning.LightningModule):
    """
    Builds a PyTorch Lightning model with
    the custom attributes and parameters
    to be inherited by the other models.
    """

    def __init__(
        self,
        learning_rate: float = 1e-3,
        units: Union[List, Tuple] = [[389, 500], [500, 500], [500, 1]],
    ):
        super().__init__()

    def training_step(
        self, batch: torch.Tensor, batch_index: int
    ) -> Tuple[float, float]:
        features, labels = batch
        logits = self(features)
        loss = self.criterion(logits, labels)
        accuracy = self.accuracy(logits.argmax(1), labels)
        self.log("train_loss", loss, prog_bar=True)
        self.log("train_accuracy", accuracy, prog_bar=True)
        return loss

    def test_step(self, batch: torch.Tensor, batch_index: int) -> Tuple[float, float]:
        features, labels = batch
        logits = self(features)
        loss = self.criterion(logits, labels)
        accuracy = self.accuracy(logits.argmax(1), labels)
        self.log("test_loss", loss, prog_bar=True)
        self.log("test_acc", accuracy, prog_bar=True)
        return loss

    def configure_optimizers(self) -> object:
        return torch.optim.SGD(
            params=self.parameters(), lr=self.learning_rate, momentum=9e-1
        )
