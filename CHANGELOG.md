# Changelog

This is an auto-generated changelog file using the `modules/generate_changelog` script. We track the notable changes to this project using the conventional commit messages in the `main` branch.

## Versions

- c2ce2a2 (tag: v0.1.0) chore: add outputs dir to .gitignore

## Chores

- 160719d - chore: Add isort config (Sat, 2 Dec 2023 13:00:58 +0800)
- 916a96c - chore: Update pre-commit config (Sat, 2 Dec 2023 13:00:51 +0800)
- 7c1a93c - chore: Accept a torch model object as feature extractor (Mon, 3 Jan 2022 13:13:22 +0800)
- 27dff23 - chore: parameterize dataset normalization (Wed, 25 Aug 2021 18:54:32 +0800)
- 6670fff - chore: add more seeds for more runs (Mon, 23 Aug 2021 22:22:05 +0800)
- 7abb350 - chore: add .idea to .gitignore (Mon, 23 Aug 2021 22:21:43 +0800)
- 8817ae6 - chore: add shebang for shell script (Mon, 23 Aug 2021 22:20:32 +0800)
- 5b91dd4 - chore: update local master branch (Mon, 23 Aug 2021 11:17:30 +0000)
- a684501 - chore: remove subnetwork implementations (Tue, 17 Aug 2021 23:35:36 +0800)
- 8ab4878 - chore: remove old classifier modules (Tue, 17 Aug 2021 16:51:23 +0800)
- c2ce2a2 - chore: add outputs dir to .gitignore (Sun, 15 Aug 2021 16:48:12 +0800)
- 2c67a58 - chore: remove cuda index in MoE device (Sun, 15 Aug 2021 01:27:02 +0800)
- 07e9d90 - chore: add shell script for training classifier (Mon, 9 Aug 2021 22:09:56 +0800)
- 32ef9c6 - chore: do not use feature extractor by default (Thu, 15 Jul 2021 22:38:36 +0800)
- 40e2561 - chore: parameterize optimizer (Wed, 14 Jul 2021 12:43:17 +0000)
- 45fd4df - chore: remove poetry (Thu, 8 Jul 2021 18:20:43 +0800)
- e22b64a - chore: use function for creating data loaders (Sun, 4 Jul 2021 20:13:22 +0800)
- 57b064f - chore: use MoE scoring function for eval (Sun, 4 Jul 2021 20:09:34 +0800)
- a02f099 - chore: parameterize the code dimensionality (Sun, 4 Jul 2021 20:01:49 +0800)
- 33d3f2f - chore: remove initialization of feature extraction layer (Sun, 4 Jul 2021 20:00:32 +0800)
- 3348f5a - chore: add inference and evaluation function (Sun, 20 Jun 2021 06:12:38 +0800)
- ded935b - chore: add validation step (Sat, 19 Jun 2021 22:57:25 +0800)
- 52dd8fd - chore: add pre-commit and flake8 config files (Thu, 17 Jun 2021 17:39:18 +0800)
- a05dd98 - chore: add parameter reset for experts (Thu, 17 Jun 2021 17:35:08 +0800)
- 8c0abd1 - chore: remove inference function (Sun, 21 Feb 2021 18:20:55 +0800)
- 69b6f50 - chore: remove draft training module (Sat, 20 Feb 2021 03:15:57 +0800)
- 35612f3 - chore: compute individual expert accuracies (Sat, 20 Feb 2021 03:15:43 +0800)
- cd61b67 - chore: use the fixed MoE for classifier examples (Sat, 20 Feb 2021 03:11:57 +0800)
- 84130f7 - chore: initialize output layer with glorot uniform initializer (Fri, 19 Feb 2021 20:08:58 +0800)
- bcd9881 - chore: initialize hidden layers with kaiming he initializer (Fri, 19 Feb 2021 20:08:42 +0800)
- ad6be02 - chore: increase gating neurons to 500 (Wed, 10 Feb 2021 12:38:00 +0800)
- 8051601 - chore: decrease to 50 neurons per expert (Tue, 9 Feb 2021 19:04:39 +0800)
- dda6701 - chore: add DNN MoE classifier (Mon, 4 Jan 2021 23:30:38 +0800)
- e16edeb - chore: parameterize expert and gating networks (Tue, 8 Dec 2020 23:23:35 +0800)
- 7cd658d - chore: add DNN class (Sun, 6 Dec 2020 14:54:06 +0800)
- 994a364 - chore: add JSON file containing hyperparameters (Sun, 8 Nov 2020 00:02:17 +0800)
- 0320712 - chore: use unpacked hyperparameters (Sat, 7 Nov 2020 23:52:18 +0800)
- 417a6ae - chore: unpack hyperparameters from JSON file (Sat, 7 Nov 2020 23:46:54 +0800)
- 68a576f - chore: add argparse param for configuration (Sat, 7 Nov 2020 23:40:36 +0800)
- 9afe5d2 - chore: parameterize LeNet layers (Sat, 7 Nov 2020 00:12:52 +0800)
- 79dcd4f - chore: add function for loading hyperparameters (Fri, 6 Nov 2020 16:03:32 +0800)
- 6ab4464 - chore: use the inference function (Fri, 6 Nov 2020 15:52:50 +0800)
- b1e8491 - chore: use argument parser (Fri, 6 Nov 2020 13:48:46 +0800)
- 36b04d3 - chore: add argument parser (Fri, 6 Nov 2020 13:47:46 +0800)
- 373a90f - chore: add main function (Thu, 5 Nov 2020 22:04:12 +0800)
- 0257e5c - chore: remove context manager for gradient calc (Thu, 5 Nov 2020 22:03:17 +0800)
- 91afc06 - chore: set global seed (Thu, 5 Nov 2020 21:21:38 +0800)
- 3c11779 - chore: add function for setting global seed (Thu, 5 Nov 2020 21:20:48 +0800)
- b776816 - chore: add module for moe classifier (Thu, 5 Nov 2020 20:34:40 +0800)
- 4b06f44 - chore: add MoE training and inference functions (Mon, 2 Nov 2020 14:04:24 +0800)
- aa04213 - chore: add Mixture-of-Experts model (Sun, 1 Nov 2020 18:57:22 +0800)
- 2fd0675 - chore: add LeNet CNN (Sun, 1 Nov 2020 18:46:39 +0800)
- a27c966 - chore: add package (Sun, 1 Nov 2020 18:34:43 +0800)
- e4384d2 - chore: add .gitignore (Sun, 1 Nov 2020 13:16:14 +0800)

## Documentation

- a6afd0d - docs: Generate initial CHANGELOG (Sat, 2 Dec 2023 13:05:40 +0800)
- b5621b6 - docs: fix typo in docstring (Wed, 21 Jul 2021 22:59:32 +0800)
- f289c4a - docs: update repository link (Wed, 21 Jul 2021 15:10:42 +0800)
- f3ba7e0 - docs: add usage notice (Thu, 8 Jul 2021 18:13:11 +0800)
- 23ad018 - docs: update MoE constructor docstring (Mon, 15 Feb 2021 22:20:35 +0800)
- e97bfa7 - docs: add release documentation (Fri, 8 Jan 2021 20:31:10 +0800)
- bd2585f - docs: add docstring for static methods (Sun, 8 Nov 2020 21:36:23 +0800)
- db634b8 - docs: add README (Sun, 8 Nov 2020 00:05:08 +0800)
- fe6d3cb - docs: add license header (Thu, 5 Nov 2020 20:42:50 +0800)
- ed7d10f - docs: add docstring for LeNet (Mon, 2 Nov 2020 13:33:12 +0800)
- 1458b4e - docs: add license (Sun, 1 Nov 2020 18:33:54 +0800)

## Features

- dcc0903 - feat: add option to normalize vision datasets (Wed, 25 Aug 2021 18:54:28 +0800)
- 275bc52 - feat: parameterize the cuda device index (Sun, 15 Aug 2021 01:20:16 +0800)
- 3734b6e - feat: parameterize number of ResNet blocks to freeze (Sun, 15 Aug 2021 01:12:25 +0800)
- 4ba01a6 - feat: add option for using pretrained CIFAR10 ResNet models (Sat, 14 Aug 2021 16:58:42 +0000)
- 7eec741 - feat: add functions for saving and loading trained models (Fri, 16 Jul 2021 08:45:37 +0000)
- 255349d - feat: add function for creating data loaders (Sun, 4 Jul 2021 20:04:33 +0800)
- 8f486c7 - feat: add function for freezing ResNet blocks (Thu, 11 Feb 2021 23:34:51 +0800)
- 8b9cf78 - feat: add ResNet18 feature extractor (Tue, 9 Feb 2021 15:30:08 +0800)
- fb72f60 - feat: add DNN feature extractor (Sat, 6 Feb 2021 22:31:39 +0800)

## Fixes

- de45250 - fix: resolve issue on accessing USPS metadata (Wed, 25 Aug 2021 10:44:11 +0000)
- b8c9bff - fix: switch to cpu if cuda device is not V100 (Mon, 23 Aug 2021 20:09:53 +0800)
- 8d9b9f2 - fix: reset params only when not using ResNet (Mon, 23 Aug 2021 19:36:34 +0800)
- 6b95e59 - fix: display correct number of subnetworks (Sat, 14 Aug 2021 20:22:00 +0000)
- 4e785ca - fix: use correct var for freezing ResNet blocks (Sat, 14 Aug 2021 20:21:30 +0000)
- fe0754c - fix: get max of accuracies (Mon, 9 Aug 2021 22:08:04 +0800)
- 5c3ca41 - fix: resolve index issue for filename splitting (Tue, 20 Jul 2021 15:20:37 +0800)
- f60e7c8 - fix: add lists for storing validation loss and acc (Sun, 4 Jul 2021 20:17:29 +0800)
- 76ca902 - fix: create an lr scheduler instance (Sun, 4 Jul 2021 20:16:57 +0800)
- 4c3a9b2 - fix: unpack metadata using meta key (Sun, 4 Jul 2021 20:15:09 +0800)
- 8f40fcf - fix: pass validation loader to training function (Sun, 4 Jul 2021 20:13:50 +0800)
- 7ac06cb - fix: resolve MoE specialization issue (Mon, 8 Feb 2021 15:38:32 +0800)

## Improvements

## Refactors

- e8d648f - refactor: Follow the classifier module pattern in kwta-ensemble (Mon, 9 Aug 2021 13:57:52 +0000)
- 3a58863 - refactor: remove commented lines (Sun, 4 Jul 2021 20:14:18 +0800)
- 1dca6cb - refactor: decouple model implementations (Thu, 1 Jul 2021 22:15:07 +0800)
- 5df2108 - refactor: convert ResNet18 feature extractor to Sequential (Sun, 20 Jun 2021 07:26:34 +0800)
- caa2cb4 - refactor: convert DNN feature extractor to Sequential (Sun, 20 Jun 2021 07:26:10 +0800)
- 77284a2 - refactor: convert CNN feature extractor to Sequential (Sun, 20 Jun 2021 07:25:50 +0800)
- 57e6066 - refactor: reorganize CNN MoE classifier (Sun, 20 Jun 2021 06:04:37 +0800)
- 524789e - refactor: reorganize DNN MoE classifier (Sun, 20 Jun 2021 06:04:10 +0800)
- bc45889 - refactor: remove unused import (Thu, 5 Nov 2020 22:04:27 +0800)
- 015a37e - refactor: rename classifier module (Thu, 5 Nov 2020 20:41:27 +0800)

## Tests
