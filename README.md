# Mixture of Experts

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![Python 3.7](https://img.shields.io/badge/python-3.7-blue.svg)](https://www.python.org/downloads/release/python-377/)
[![Python 3.8](https://img.shields.io/badge/python-3.8-blue.svg)](https://www.python.org/downloads/release/python-382/)

_This is an implementation of the proposed model in Adaptive Mixture of Local
Experts (Jacobs, Jordan, Nowlan, and Hinton, 1991)._

![](https://www.researchgate.net/profile/Christian_Thurau2/publication/228933107/figure/fig1/AS:669500202557453@1536632695689/A-mixture-of-experts-architecture-consists-of-a-set-of-expert-networks-and-a-gating.png)

## Overview

The Mixture-of-Expers model is a system of machine learning models where
multiple experts (sub-models) are used to handle a subset of the complete
dataset, in which it assumes the dataset is comprised of homogenous regions.
To determine which expert to use for a specific input case, a managing network
called the _gating network_ is used. In this system, the experts should only
learn the cases where they are good at modelling, and
ignore those cases that they are not performing well.

## Usage

Install the dependencies of the package, preferably on a virtual environment.

```shell script
$ virtualenv venv --python=python3
$ pip install -r requirements.txt
```

This package consists of the following models as experts for MoE:

- Feed-Forward Neural Network (customizable)
- Convolutional Neural Network (LeNet architecture)

The sample models are located at `modules` directory. This is an example of
using a DNN-based MoE,

```python
from moe.models import DNN, MoE


epochs = 25
num_experts = 3
expert_model = DNN(
    units=((784, 500), (500, 500), (500, 10))
)
gating_model = DNN(
    units=((784, 512), (512, num_experts))
)
model = MoE(
    input_shape=1,
    expert_model=expert_model,
    gating_model=gating_model,
    num_experts=num_experts,
    output_dim=10,
)
model.fit(train_loader, epochs=epochs, show_every=5)
```

## License

This repository is available for non-commercial research purposes only.

```
Implementation of Adaptive Mixture of Experts
Copyright (C) 2020--present  Abien Fred Agarap

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
