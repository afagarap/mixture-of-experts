# Implementation of Adaptive Mixture of Experts
# Copyright (C) 2020--present  Abien Fred Agarap
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Unit test for MoE"""
import torch
from sklearn.datasets import load_digits

from moe.models.moe import MoE

features, labels = load_digits(return_X_y=True)
features, labels = features[:32], labels[:32]


# last element of last element is num_classes
units = [[64, 100], [100, 100], [100, 10]]
model = MoE(units=units)


def test_forward():
    batch = torch.from_numpy(features).float()
    logits = model(batch)
    assert logits.shape[1] == units[-1][-1]
